= Welcome to Component C!
include::../_attributes.adoc[]

// Settings
:idprefix:
:idseparator: -

This is the automatic start page for version *{page-component-version}* of Component C.

Component C includes files from other modules in Antora Demo.

